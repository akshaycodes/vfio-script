#!/bin/bash

if test -e /etc/libvirt/ && ! test -e /etc/libvirt/hooks;
then
   mkdir -p /etc/libvirt/hooks;
fi
if test -e /etc/libvirt/hooks/qemu;
then
    mv /etc/libvirt/hooks/qemu /etc/libvirt/hooks/qemu_last_backup
fi
if test -e /bin/vfio-script.sh;
then
    mv /bin/vfio-script.sh /bin/vfio-script.sh.bkp
fi

if test -e /etc/systemd/system/libvirt-nosleep@.service;
then
    rm /etc/systemd/system/libvirt-nosleep@.service
fi

cp -v systemd-no-sleep/libvirt-nosleep@.service /etc/systemd/system/libvirt-nosleep@.service
cp -v base/vfio-script.sh /bin/vfio-script.sh
cp -v base/qemu /etc/libvirt/hooks/qemu

chmod +x /bin/vfio-script.sh
chmod +x /etc/libvirt/hooks/qemu
#libvirt-nosleep@ (adapted from https://gitlab.com/risingprismtv/single-gpu-passthrough)
