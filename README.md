# Read Me 

##  VFIO-Script

This Script is design for AMD GPU user who are suffering from the reset bug.


1. Change the VM name in qemu if not already win10
2. Installation 
    ```bash
    cd vfio-scrip
    sudo bash vfio_script_install.sh
    ```

### FAQ
1. My computer when to sleep/shutdown?

    ANS: Script is design to do to put your system into sleep and allow GPU to properly reset. In some case you have to press the power button.
2. Can I wake my computer via keyboard and mouse?

    ANS: YES, Check your bios setting for that. 
3. Do I have to edit this script to work for my system?

    ANS: No, The script design to automatically detect AMD GPU but if you have two AMD GPU and you want to select 2nd gpu then you have make few changes in #VAR section of the script
4. Can I use this script for multiple VMs?

    ANS: Yes..just edit this file base/qemu
    ```bash
        #add your vm name  like the following
        # In Bash "||" this represent and 
         $OBJECT == "win10" || "Macos" || "FreeBSD" 
    ```
